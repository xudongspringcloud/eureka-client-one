package com.example.eurekaclient.controller;

import com.example.eurekaclient.dao.RepoMapper;
import com.example.eurekaclient.model.RepoInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 提供库存服务
 */
@RestController
public class RepoController {
    @Autowired
    DiscoveryClient discoveryClient;
    @Resource
    RepoMapper repoMapper;
    @PostMapping("/productRepo")
    public String productRepo(@RequestParam("productId") String productId,@RequestParam("amount") int amount) {
        String services = "Services: " + discoveryClient.getServices(); // 调用本服务的消费者
        System.out.println("调用本服务的消费方："+services);
        System.out.println("==================> 的更新库存信息接口被调用,参数：productId="+productId+",amount="+amount);
        String result="";
        int count = repoMapper.getAmount(productId); // 当前库存
        if(count>amount){
            RepoInfo repoInfo = new RepoInfo();
            repoInfo.setProductId(productId);
            repoInfo.setAmount(count-amount);
            repoMapper.update(repoInfo);
            amount = repoMapper.getAmount(productId);
            System.out.println("更新后库存为："+amount);
            result=String.valueOf(amount);
        }else{
            result = "fail";
        }
        return result;
    }

}
